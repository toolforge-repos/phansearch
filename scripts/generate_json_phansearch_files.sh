#!/bin/sh

# This launches the PhanSearch analysis: it installs MediaWiki, extensions, skins, Phan, the Phan plugin, then launches Phan

#
# Parameters
#

# Debug this tool itself
# It reuses the MediaWiki installation instead of updating it and only the file PhanSearchPlugin.php is updated
# (although it gets updated when there is no 'vendor' subdirectory (light updating in debug mode))
DEBUG=0

# Verbose script
VERBOSE=1

# Work directory
WORKDIR='./public_html/data'

# MediaWiki branch
BRANCH='master,REL1_38,REL1_39'

for param in $*; do
	case "$param" in
		--debug) DEBUG=1 ;;
		--verbose) VERBOSE=1 ;;
		--workdir=*) WORKDIR=`echo -n "$param"|cut -c11-` ;;
		--branch=*) BRANCH=`echo -n "$param"|cut -c10-` ;;
		*) echo 'Error: bad parameter'; exit 1 ;;
	esac
done



#
# Code
#

set -eu
export IFS="
"

dir="`pwd -L`/$0"
dir=`dirname "$dir"`
dir=`dirname "$dir"`

if [ "$WORKDIR" = './public_html/data' ]; then
	WORKDIR="$dir/public_html/data"
fi

BRANCH=`echo -n "$BRANCH"|sed 's/,/\n/g'`

for branch in $BRANCH; do

	if [ "$DEBUG" = 0 -o ! -d "$WORKDIR/mediawiki/$branch" -o ! -d "$WORKDIR/mediawiki/$branch/vendor" ]; then

		mkdir -p "$WORKDIR/cache"
		if [ -f list-extensions.txt ]; then
			repos=`cat list-extensions.txt`
		else
			if which wget >/dev/null; then
				wget --quiet --output-document="$WORKDIR/cache/settings.yml.b64" "https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/tools/release/+/refs/heads/master/make-release/settings.yaml?format=TEXT"
			elif which curl >/dev/null; then
				curl -s "https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/tools/release/+/refs/heads/master/make-release/settings.yaml?format=TEXT" >"$WORKDIR/cache/settings.yml.b64"
			else
				exit 1
			fi
			base64 -d "$WORKDIR/cache/settings.yml.b64" >"$WORKDIR/cache/settings.yml" 2>/dev/null || mv cache/settings.yml.b64 cache/settings.yml # As of 2022-08-25 Gerrit returns a base64-encoded text, but it is expected to be fixed at some time
			repos=`cat "$WORKDIR/cache/settings.yml"|grep -F -B10000 wmf_core:|grep -F -A10000 base:|grep -F mediawiki|grep -E -o '(extensions/[^"]*|skins/[^"]*|vendor)'`
		fi

		# Clone MediaWiki
		if [ ! -d "$WORKDIR/mediawiki/$branch" ]; then
			[ "$VERBOSE" = "1" ] && echo -n "Clone MediaWiki…"
			git clone --quiet --depth 1 --branch "$branch" --no-tags --reference-if-able "/var/cache/git/mediawiki/core.git" https://gerrit.wikimedia.org/r/mediawiki/core.git "$WORKDIR/mediawiki/$branch"
		else
			[ "$VERBOSE" = "1" ] && echo -n "Update MediaWiki…"
			git -C "$WORKDIR/mediawiki/$branch" clean --quiet -fxd
			git -C "$WORKDIR/mediawiki/$branch" fetch --quiet --all
			git -C "$WORKDIR/mediawiki/$branch" merge --quiet --ff-only
		fi
		[ "$VERBOSE" = "1" ] && echo " done"

		[ "$VERBOSE" = "1" ] && echo -n "Clone or update extensions and skins…"
		cd "$WORKDIR/mediawiki/$branch"
		if [ "$branch" = 'master' ]; then
			for repo in $repos; do
				if [ ! -d "$WORKDIR/mediawiki/$branch/$repo" ]; then
					git clone --quiet --depth 1 --branch "$branch" --no-tags --reference-if-able "/var/cache/git/mediawiki/$repo.git" "https://gerrit.wikimedia.org/r/mediawiki/$repo.git" "$repo"
				else
					git -C "$WORKDIR/mediawiki/$branch/$repo" clean --quiet -fxd
					git -C "$WORKDIR/mediawiki/$branch/$repo" reset --hard HEAD
					git -C "$WORKDIR/mediawiki/$branch/$repo" fetch --quiet --all
					git -C "$WORKDIR/mediawiki/$branch/$repo" merge --quiet --ff-only
				fi
				git -C "$WORKDIR/mediawiki/$branch/$repo" log -1 --pretty=fuller >"$WORKDIR/mediawiki/$branch/$repo/.git/COMMIT"
			done
		else
			git submodule foreach 'git clean --quiet -fxd'
			git submodule foreach 'git reset --hard HEAD'
			git submodule update --init --recursive
		fi
		[ "$VERBOSE" = "1" ] && echo " done"

		# Install PhanSearch
		[ "$VERBOSE" = "1" ] && echo -n "Install PhanSearch Phan plugin…"
		mkdir -p ".phan/plugins"
		cp -a "$dir/phan/PhanSearchPlugin.php" ".phan/plugins/PhanSearchPlugin.php"
		cp -a "$dir/phan/PhanSearchUtils.php" ".phan/plugins/PhanSearchUtils.php"
		[ "$VERBOSE" = "1" ] && echo " done"

		# Configure Phan
		[ "$VERBOSE" = "1" ] && echo -n "Configure Phan…"
		echo "<?php\n" > ".phan/local-config.php"
		echo "\$cfg['directory_list'][] = 'extensions';" >> ".phan/local-config.php"
		echo "\$cfg['directory_list'][] = 'skins';" >> ".phan/local-config.php"
		echo "\$cfg['plugins'] = [ '.phan/plugins/PhanSearchPlugin.php' ];" >> ".phan/local-config.php"
		echo "# Deactivate all Phan standard analysis" >> ".phan/local-config.php"
		echo "\$cfg['whitelist_issue_types'] = [ 'PhanVoid' ];" >> ".phan/local-config.php"
		[ "$VERBOSE" = "1" ] && echo " done"

		# Run Composer
		[ "$VERBOSE" = "1" ] && echo "Run Composer…"
		[ -f "composer.phar" ] || curl -s 'https://getcomposer.org/download/2.4.4/composer.phar' >"composer.phar"
		php composer.phar install || /bin/true
		[ "$VERBOSE" = "1" ] && echo

	else
		cd "$WORKDIR/mediawiki/$branch"
		cp -a "$dir/phan/PhanSearchPlugin.php" ".phan/plugins/PhanSearchPlugin.php"
		cp -a "$dir/phan/PhanSearchUtils.php" ".phan/plugins/PhanSearchUtils.php"
	fi

	commit=`git -C "$WORKDIR/mediawiki/$branch" rev-parse HEAD`
	git -C "$WORKDIR/mediawiki/$branch" log -1 --pretty=fuller >"$WORKDIR/mediawiki/$branch/.git/COMMIT"
	if [ "$DEBUG" = 1 -o ! -d "$WORKDIR/calls/$branch/$commit" ]; then

		# Run Phan
		[ "$VERBOSE" = "1" ] && echo "Run Phan…"
		php vendor/bin/phan -d . --long-progress-bar --allow-polyfill-parser

		# Re-create the branches.json file
		list=`ls "$WORKDIR/mediawiki"`
		echo -n '{"git":[' >"$WORKDIR/branches.json"
		first=0
		for l in $list; do
			if [ "$first" = 1 ]; then
				echo -n ',' >>"$WORKDIR/branches.json"
			fi
			echo -n '"'"$l"'"' >>"$WORKDIR/branches.json"
			first=1
		done
		echo ']}' >>"$WORKDIR/branches.json"

		# Add a file metadata.json
		date=`grep -E -o 'CommitDate: .+' "$WORKDIR/mediawiki/$branch/.git/COMMIT"|cut -c13-`
		echo -n "{\"commit\":\"$commit\",\"date\":\"$date\"}" >"$WORKDIR/calls/$branch/$commit/metadata.json"
	fi

done

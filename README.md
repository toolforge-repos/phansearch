PhanSearch
==========

This plugin for the PHP static analysis tool [Phan](https://www.mediawiki.org/wiki/Phan) creates a **database of all function calls in MediaWiki and its extensions**. The goal is to help MediaWiki developers to explore and understand more easily the codebase.

The Phan plugin itself collect data during Phan analysis, then stores this database as JSON files.

The associated user interface is a simple HTML+JS and let the user display all function calls for a given class and method.

Features
--------

Data collecting program:
* Collect PHP function calls, as well as function definitions
* Stored as JSON files (one JSON file per class)

User interface:
* Autocompletion for class and method
* Display function definition and associated calls
* Precise line delimitations (start and end)
* Links to Gerrit and Github

Ideas
-----

See also [the page on Wiktech](https://wikitech.wikimedia.org/wiki/Tool:PhanSearch#TODO).

* Tag deprecated functions and add a predefined search to choose a deprecated function (in order to create the patches to remove these function calls)
* If possible, tag partly-deprecated functions, for instance when the semantic of a parameter changes, this would be when a function contains `wfDeprecated()` (or similar, investigation needed)
* Filter deprecated function calls for one extension, for extension maintainers wanting to resolve technical debt
* For abstract functions, display all corresponding concrete functions
* More generally, a better behaviour when a function is overloaded (currently it seems sometimes the abstract method is selected sometimes it is the concrete function (depending on the code doc?))
* A better switch for the MediaWiki version

Licence
-------

[Unlicense](https://unlicense.org)

var versions = {}, dates = {}, classes = {}, methods = {}, classesByInterface = {}, fullMethods = [], definitions = {}, calls = {}, autocomplete = null;
var options = {
	branch: 'master',
	links: gerritLink,
	abstract_methods: 'only',
	context: 3,
};

function installAutocompleteClasses( list ) {
	if( autocomplete !== null ) {
		$( '#search' ).autocomplete( 'destroy' );
	}
	$( '#search' ).autocomplete( {
		source: list,
		delay: 50,
		select: function() { downloadMethodsAndInstallAutocompleteMethods( $( '.ui-helper-hidden-accessible' ).text() ) },
	} );
	autocomplete = 'classes';
}

function installAutocompleteMethods( list ) {
	if( autocomplete !== null ) {
		$( '#search' ).autocomplete( 'destroy' );
	}
	$( '#search' ).autocomplete( {
		source: list,
		delay: 0,
		select: function() { downloadMethodCallsAndRender( $( '.ui-helper-hidden-accessible' ).text() ) },
	} );
	autocomplete = 'methods';
	$( '#search' ).autocomplete( 'search' );
}

function downloadMethodsAndInstallAutocompleteMethods( className ) {
	if( !className ) {
		className = $( '#search' ).val();
	}
	downloadMethods( [ className ], installAutocompleteMethods );
}
function downloadMethods( classNames, callback ) {
	if( classNames.length === 0 || !classes || !classes['all'] ) {
		return;
	}
	var className = classNames.pop();
	if( !className || !classes['all'].includes( className ) || methods[className] ) {
		return;
	}
	var pathClassName = className.replaceAll( '\\', '/' );
	$.getJSON( 'data/calls/' + options.branch + '/' + versions[options.branch][0] + '/by-class/' + pathClassName + '/.metadata.json' ).done( function( data ) {
		//$( '#search' ).autocomplete( 'destroy' );
		methods[className] = data['methods'];
		data['methods'] = data['methods'].map( function( x ) { return className + '::' + x; } );
		fullMethods = data['methods'];
		if( data['implementations'] && data['implementations'].length ) {
			classesByInterface[className] = data['implementations'];
			downloadMethods( classNames.concat( data['implementations'] ) );
		} else if( classNames.length ) {
			downloadMethods( classNames );
		}
		if( callback ) {
			callback( fullMethods );
		}
	} );
}
function downloadMethodCallsAndRender( methodName ) {
	if( !methodName ) {
		methodName = $( '#search' ).val();
	}
	downloadMethodCalls( [ methodName ], render );
}
function downloadMethodCalls( methodNames, callback ) {
	if( methodNames.length === 0 ) {
		return;
	}
	var methodName = methodNames.pop();
	if( fullMethods.includes( methodName ) ) {
		var className = methodName.replace( /^([^:]+)::.+$/, '$1' );
		var pathMethodName = methodName.replaceAll( '\\', '/' ).replaceAll( '::', '/' );
		if( !definitions[methodName] ) {
			$.getJSON( 'data/calls/' + options.branch + '/' + versions[options.branch][0] + '/by-class/' + pathMethodName + '.json' ).done( function( data ) {
				$( '#search' ).autocomplete( 'close' );
				definitions[methodName] = data['definition'];
				calls[methodName] = data['calls'];
				//console.log( definitions[methodName] );
				//console.log( calls[methodName] );
				//$( '#search' ).autocomplete( 'destroy' );
				if( classesByInterface[className] && classesByInterface[className].length ) {
					downloadMethodCalls( methodNames.concat( classesByInterface[className] ) );
				}
				if( callback ) {
					callback( methodName );
				}
			} );
		} else if( callback ) {
			render( methodName );
		}
	}
}

function render( methodName ) {
	renderDefinition( definitions[methodName] );
	renderCalls( calls[methodName], methodName );
}

function renderDefinition( definitionData ) {

	var multipleLines = definitionData['line_end'] !== null && definitionData['line_start'] !== definitionData['line_end'],
	    gerrit = gerritLink( definitionData['file'], versions[options.branch][0], definitionData['line_start'] ),
	    github = githubLink( definitionData['file'], versions[options.branch][0], definitionData['line_start'], definitionData['line_end'] );

	$( '#definition' ).html(
		'<h1>Definition</h1>' +
		'<h2>' +
			'<code>' + definitionData['file'] + '</code>' +
		'</h2>' +
		//'<ul>' +
		//	'<li>Class: <code>' + definitionData['class'] + '</code></li>' +
		//	'<li>Method: <code>' + definitionData['method'] + '</code></li>' +
		//	'<li>File: <code>' + definitionData['file'] + '</code> (line' + ( multipleLines ? 's' : '' ) + ' ' + definitionData['line_start'] + ( multipleLines ? ' to ' + definitionData['line_end'] : '' ) + ') (<a href="' + gerrit + '">Gerrit</a>) (<a href="' + github + '">Github</a>)</li>' +
		//'</ul>' +
		'<div class="codeDefinition">' +
			renderCode( definitionData['code'], definitionData['line_before_capture'], definitionData['line_after_capture'], definitionData['line_start'], definitionData['file'] ) +
		'</div>'
	);
}

function renderCall( callData ) {
	var multipleLines = callData['line_end'] !== null && callData['line_start'] !== callData['line_end'],
	    gerrit = gerritLink( callData['file'], versions[options.branch][0], callData['line_start'] ),
	    github = githubLink( callData['file'], versions[options.branch][0], callData['line_start'], callData['line_end'] );
	return '<div class="call">' +
			//'<ul>' +
			//	'<li>File: <code>' + callData['file'] + '</code> (line' + ( multipleLines ? 's' : '' ) + ' ' + callData['line_start'] + ( multipleLines ? ' to ' + callData['line_end'] : '' ) + ') (<a href="' + gerrit + '">Gerrit</a>) (<a href="' + github + '">Github</a>)</li>' +
			//'</ul>' +
			'<div class="codeCall">' +
				renderCode( callData['code'], callData['line_before_capture'], callData['line_after_capture'], callData['line_start'], callData['file'], callData['lines_before3'], callData['lines_after3'] ) +
			'</div>' +
		'</div>';
}

function renderCallFile( filename, callData ) {
	return '<div class="file">' +
			'<h2>' +
				'<code>' + filename + '</code>' +
			'</h2>' +
			'<div class="codeCall">' +
				callData.map( renderCall ).join( '\n' ) +
			'</div>' +
		'</div>';
}

function renderCalls( callsData, methodName ) {
	var className = methodName.replace( /^([^:]+)::.+$/, '$1' );
	    methodNameStrict = methodName.replace( /^[^:]+::(.+)$/, '$1' );
	var callsByFile = callsData.reduce( function( acc, x ) {
		if( !acc[x['file']] ) {
			acc[x['file']] = [];
		}
		acc[x['file']].push( x );
		acc[x['file']].sort( function( a, b ) {
			return a['line_start'] - b['line_start'];
		} );
		return acc;
	}, {} ), nbFiles = Object.getOwnPropertyNames( callsByFile ).length;
	if( classesByInterface[className] && classesByInterface[className].length ) {
		var implementations = classesByInterface[className].filter( function( x ) {
			return fullMethods.includes( x + '::' + methodNameStrict );
		} ), otherMethodNames = classesByInterface[className].map( function( x ) {
			return x.substr( 1 ) + '::' + methodNameStrict; // remove the initial "\" in class name
		} );
		var callsThroughInterface = otherMethodNames.reduce( function( globalAcc, otherMethodName ) {
			if( !otherMethodName ) {
				return globalAcc;
			}
			if( !calls[otherMethodName] ) {
				return globalAcc;
			}
			return globalAcc.concat( calls[otherMethodName] );
		}, [] );
		var callsThroughInterfaceByFile = otherMethodNames.reduce( function( globalAcc, otherMethodName ) {
			if( !otherMethodName ) {
				return globalAcc;
			}
			if( !calls[otherMethodName] ) {
				return globalAcc;
			}
			return calls[otherMethodName].reduce( function( acc, x ) {
				if( !acc[x['file']] ) {
					acc[x['file']] = [];
				}
				acc[x['file']].push( x );
				acc[x['file']].sort( function( a, b ) {
					return a['line_start'] - b['line_start'];
				} );
				return acc;
			}, globalAcc );
		}, {} ), nbOtherFiles = Object.getOwnPropertyNames( callsThroughInterfaceByFile ).length;
	}
	$( '#calls' ).html(
		'<h1>Calls' + ( definitions[methodName].isInterface ? ' directly attributed to the interface method' : '' ) + '</h1>' +
		'<span id="numberResults">' + callsData.length + ' result' + ( callsData.length > 1 ? 's' : '' ) + ' in ' + nbFiles + ' file' + ( nbFiles > 1 ? 's' : '' ) + '</span>' +
		//callsData.map( renderCall ).join( '\n' )
		Object.getOwnPropertyNames( callsByFile ).sort().map( function( x ) {
			return renderCallFile( x, callsByFile[x] );
		} ).join( '\n' ) +
		( definitions[methodName].isInterface ?
			'<h1>Calls through the implemented methods</h1>' +
			'<span id="numberOtherResults">' + callsThroughInterface.length + ' result' + ( callsThroughInterface.length > 1 ? 's' : '' ) + ' in ' + nbOtherFiles + ' file' + ( nbOtherFiles > 1 ? 's' : '' ) + '</span>' +
			( classesByInterface[className] && classesByInterface[className].length ?
				//callsThroughInterface.map( renderCall ).join( '\n' )
				Object.getOwnPropertyNames( callsThroughInterfaceByFile ).sort().map( function( x ) {
					return renderCallFile( x, callsThroughInterfaceByFile[x] );
				} ).join( '\n' )
			: '' )
		: '' )
	);
}

function repoFromFilename( filename, core ) {
	var res;
	if( res = filename.match( /^(extensions\/[^/]+)\/(.+)/ ) ) {
		return [ '/' + res[1], res[2] ];
	}
	return [ core, filename ];
}

function gerritLink( filename, version, line_start ) {
	var repo = repoFromFilename( filename, '/core' );
	return 'https://gerrit.wikimedia.org/g/mediawiki' + repo[0] + '/+/' + version + '/' + repo[1] + ( line_start ? '#' + line_start : '' );
}

function githubLink( filename, version, line_start, line_end ) {
	var repo = repoFromFilename( filename, '' );
	return 'https://github.com/wikimedia/mediawiki' + repo[0].replaceAll( '/', '-' ) + '/blob/' + version + '/' + repo[1] + ( line_start ? '#L' + line_start + ( line_end && line_start !== line_end ? '-L' + line_end : '' ) : '' );
}

function gerritLinkA( filename, version, line ) {
	return '<a href="' + gerritLink( filename, version, line ) + '">' + line + '</a>';
}

function githubLinkA( filename, version, line ) {
	return '<a href="' + githubLink( filename, version, line ) + '">' + line + '</a>';
}

function codeLinkA( filename, version, line ) {
	return '<a href="' + options.links( filename, version, line ) + '">' + line + '</a>';
}

function renderCode( code, line_before_capture, line_after_capture, line_start, filename, lines_before3, lines_after3 ) {
	if( code === null ) {
		return '(no code)';
	}
	if( line_start === undefined ) {
		line_start = 1;
	}
	code = ( line_before_capture ? line_before_capture : '' ) + code + ( line_after_capture ? line_after_capture : '' );
	var nb_lines_before3 = 0, nb_lines_code = code.split( '\n' ).length, nb_lines_after3 = 0;
	var html_lines_before3 = '', html_lines_after3 = '';

	if( lines_before3 ) {
		nb_lines_before3 = lines_before3.split( '\n' ).length;
		html_lines_before3 = lines_before3.split( '\n' ).map( function( x, i ) {
			return '<span class="context-line"><span class="lineNumber">' + codeLinkA( filename, versions[options.branch][0], line_start-nb_lines_before3+i ) + '</span>' + x + '</span>';
		} ).join( '\n' ) + '\n';
	}
	if( lines_after3 ) {
		nb_lines_after3 = lines_after3.split( '\n' ).length;
		html_lines_after3 = '\n' + lines_after3.split( '\n' ).map( function( x, i ) {
			return '<span class="context-line"><span class="lineNumber">' + codeLinkA( filename, versions[options.branch][0], line_start+nb_lines_code+i ) + '</span>' + x + '</span>';
		} ).join( '\n' );
	}
	return '<div class="code">' +
		html_lines_before3 +
		code.split( '\n' ).map( function( x, i ) {
			return '<span class="highlight-line"><span class="lineNumber">' + codeLinkA( filename, versions[options.branch][0], i+line_start ) + '</span>' + x + '</span>';
		} ).join( '\n' ) +
		html_lines_after3 +
		'</div>';
}

function downloadInitialData( branch ) {
	return $.getJSON( 'data/calls/' + branch + '/versions.json', function( data_version ) {
		versions[branch] = data_version['versions'];
	} ).then( function() {
		$.getJSON( 'data/calls/' + branch + '/' + versions[branch][0] + '/metadata.json', function( data_metadata ) {
			dates[branch] = {};
			dates[branch][versions[branch][0]] = data_metadata['date'];
			$.getJSON( 'data/calls/' + branch + '/' + versions[branch][0] + '/classes.json', function( data ) {
			//$.ajax( { url: 'data/calls/' + versions[branch][0] + '/classes.json', dataType: 'json', headers: { Accept: 'application/json' }, success: function( data ) {
				//console.log( data );
				data['MediaWiki'] = data['MediaWiki'].map( function( x ) { return x.replaceAll( '/', '\\' ); } );
				data['extensions'] = data['extensions'].map( function( x ) { return x.replaceAll( '/', '\\' ); } );
				data['skins'] = data['skins'].map( function( x ) { return x.replaceAll( '/', '\\' ); } );
				data['vendor'] = data['vendor'].map( function( x ) { return x.replaceAll( '/', '\\' ); } );
				data['all'] = data['MediaWiki'].concat( data['extensions'], data['skins'], data['vendor'] );
				classes = data;
				methods = {};
				fullMethods = [];
				definitions = {};
				calls = {};
				installAutocompleteClasses( classes['MediaWiki'] );
			} ).fail( function() {
				console.error( 'Error when receiving the list of classes' );
			} );
		} );
	} );

}

$( function() {
	$.getJSON( 'data/calls/branches.json', function( data_branches ) {
		data_branches.git.forEach( function( branch, i ) {
			$( '#option-version' ).append( ( i > 0 ? ', ' : '' ) + '<a id="option-version-' + branch + '">' + branch + '</a>' );
			$.getJSON( 'data/calls/' + branch + '/versions.json', function( data_version ) {
				versions[branch] = data_version['versions'];
				$.getJSON( 'data/calls/' + branch + '/' + versions[branch][0] + '/metadata.json', function( data_metadata ) {
					dates[branch] = {};
					dates[branch][versions[branch][0]] = data_metadata['date'];
					//$( '#option-version-' + branch ).attr( 'data-version', branch ).attr( 'href', options.links( '', versions[branch][0] ) ).text( branch + ' (' + versions[branch][0].substr( 0, 9 ) + ')' ).click( function() {
					$( '#option-version-' + branch ).css( 'cursor', 'pointer' ).css( 'font-weight', options.branch === branch ? 'bold' : 'normal' ).attr( 'data-version', branch ).text( branch + ' (' + versions[branch][0].substr( 0, 9 ) + ')' ).attr( 'title', dates[branch] && dates[branch][versions[branch][0]] ? dates[branch][versions[branch][0]] : null ).click( function() {
						$( '#option-version-' + options.branch ).css( 'font-weight', 'normal' );
						options.branch = branch;
						console.log( 'Switch to branch ' + branch );
						$(this).css( 'font-weight', 'bold' );
						$( '#definition' ).html( '' );
						$( '#calls' ).html( '' );
						$( '#search' ).autocomplete( 'destroy' );
						autocomplete = null;
						$( '#search' ).val( '' );
						downloadInitialData( options.branch );
						return false;
					} );
				} );
			} );
		} );
	} ).then( function() {
		downloadInitialData( options.branch );
	} );
	//$( '#search' ).keyup( function( e ) {
	$( '#option-links' ).click( function( e ) {
		var currentValue = $( e.currentTarget ).text();
		if( currentValue === 'Gerrit' ) {
			$( e.currentTarget ).text( 'Github' );
			options.links = githubLink;
			$( '.lineNumber a' ).each( function( x ) {
				var href = $( this ).attr( 'href' );
				href = href.replace( /^https:\/\/gerrit\.wikimedia\.org\/g\/mediawiki\/(core|(?:extensions|skins)\/[^/]+)\/\+\/([a-z0-9]+)\/([^#]+)#([0-9]+)$/, function( match, p1, p2, p3, p4 ) {
					var repo = p1 === 'core' ? '' : p1 + '/';
					return githubLink( repo + p3, p2, p4 );
				} );
				$( this ).attr( 'href', href );
			} );
		} else {
			$( e.currentTarget ).text( 'Gerrit' );
			options.links = gerritLink;
			$( '.lineNumber a' ).each( function( x ) {
				var href = $( this ).attr( 'href' );
				href = href.replace( /^https:\/\/github\.com\/wikimedia\/mediawiki((?:-(?:extensions|skins)-[^/]+)?)\/blob\/([a-z0-9]+)\/([^#]+)#L([0-9]+)$/, function( match, p1, p2, p3, p4 ) {
					var repo = p1 === '' ? '' : p1.replaceAll( '-', '/' ).substr( 1 ) + '/';
					return gerritLink( repo + p3, p2, p4 );
				} );
				$( this ).attr( 'href', href );
			} );
		}
		return false;
	} );
	$( '#search' ).on( 'input', function( e ) {
		//$( '#resultsbox' ).html( $( '#search' ).val() );
		var value = $( '#search' ).val();
		//console.log( value, classes['all'].includes( value ) );
		if( !value ) {
			$( '#definition' ).html( '' );
			$( '#calls' ).html( '' );
			installAutocompleteClasses( classes['MediaWiki'] );
		}
		if( classes && classes['all'] && classes['all'].includes( value ) && !methods[value] ) {
			downloadMethodsAndInstallAutocompleteMethods( value );
		}
		if( fullMethods.includes( value ) && !definitions[value] ) {
			downloadMethodCallsAndRender( value );
		} else if( fullMethods.includes( value ) ) {
			downloadMethodCallsAndRender( value );
		}
	} );
} );

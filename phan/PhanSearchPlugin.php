<?php

declare(strict_types=1);

namespace Phan\Plugin\PhanSearch;

use ast\Node;
use Phan\AST\ASTReverter;
use Phan\CodeBase;
use Phan\Issue;
use Phan\Language\Context;
use Phan\Language\Element\Func;
use Phan\Language\Element\FunctionInterface;
use Phan\Language\Element\Method;
use Phan\Language\FQSEN\FullyQualifiedClassConstantName;
use Phan\Language\FQSEN\FullyQualifiedClassName;
use Phan\Language\FQSEN\FullyQualifiedMethodName;
use Phan\Library\StringUtil;
use Phan\PluginV3;
use Phan\PluginV3\AnalyzeFunctionCapability;
use Phan\PluginV3\AnalyzeMethodCapability;
use Phan\PluginV3\BeforeAnalyzePhaseCapability;

require_once __DIR__ . '/PhanSearchUtils.php';

class PhanSearchPlugin extends PluginV3 implements BeforeAnalyzePhaseCapability, AnalyzeFunctionCapability, AnalyzeMethodCapability {

	public $code_base_directory = '';
	public $calls_directory = '';
	public $base_directory = '';
	public $version = null;

	public $seenContexts = [];
	public $class_names = [];
	public $method_names = [];
	public $classesByInterface = [];

	public function __construct() {

		$this_directory = __DIR__; // We are in $WORKDIR/mediawiki/$BRANCH/.phan/plugins
		if( preg_match( '/(.+)\/mediawiki\/([^\/]+)\/\.phan\/plugins$/', $this_directory, $matches ) ) {
			$workdir = $matches[1];
			$branch = $matches[2];
			$this->code_base_directory = $workdir . '/mediawiki/' . $branch;
			$this->base_directory = $workdir . '/calls/' . $branch;
			$this->calls_directory = $this->base_directory;
		} else {
			throw new \Exception( 'Wrong path' );
		}

		$this->version = Utils::getGitHash( $this->code_base_directory );
		if( $this->version !== null ) {
			$this->calls_directory .= '/' . $this->version;
		}

		$this->class_names = [
			'MediaWiki' => [],
			'vendor' => [],
			'extensions' => [],
			'skins' => [],
		];
	}

	public function __destruct() {

		$versions = $this->version ? [ $this->version ] : [];
		file_put_contents( $this->base_directory . '/versions.json', json_encode( [ 'versions' => $versions ] ) );
		file_put_contents( $this->calls_directory . '/classes.json', json_encode( $this->class_names ) );
		foreach( $this->method_names as $class => $methods ) {
			$implementations = array_key_exists( '\\' . $class, $this->classesByInterface ) ? $this->classesByInterface['\\' . $class] : [];
			$json = [
				'methods' => $methods,
				'implementations' => $implementations,
			];
			file_put_contents( $this->calls_directory . '/by-class/' . $class . '/.metadata.json', json_encode( $json ) );
		}
	}

	/**
	 * Save function call in the database.
	 *
	 * @param CodeBase $code_base Code base to be analyzed
	 * @param Context $context Context of the function call
	 * @param FunctionInterface $function Function
	 * @param array $args Arguments
	 * @param ?Node $node Node
	 * @return array
	 */
	public function saveFunctionCall( CodeBase $code_base, Context $context, FunctionInterface $function, array $args, ?Node $node ) {

		// Get FQSEN informations
		// NOTE: $function is the instance of the method effectively in this class, but if it is not explicitely defined here,
		// $method_definition is the effective definition just above in the class hierarchy. This definition could override
		// other methods with the same name higher in the class hierarchy.
		#$method_fqsen = $function->getFQSEN();
		$method_fqsen = $function->getContext()->getFunctionLikeFQSEN();
		$method_definition = $function->getContext()->getFunctionLikeInScope( $code_base );
		$is_method = $method_definition instanceof Method;
		#$canonical_method_fqsen = $method_fqsen instanceof FullyQualifiedMethodName ? $method_fqsen->getCanonicalFQSEN() : $method_fqsen;
		#$canonical_method_fqsen = $method_fqsen->getCanonicalFQSEN();
		$method_name = $method_fqsen->getName();
		$class_fqsen = $is_method ? $method_fqsen->getFullyQualifiedClassName() : null;
		$class_name = $is_method ? preg_replace( '/^\//', '', str_replace( '\\', '/', (string) $class_fqsen ) ) : null;
		$class = $is_method ? $method_definition->getClass( $code_base ) : null;
		$uniqueKey = ((string) $context) . ':' . ((string) $method_fqsen);
		$file_name = $function->getContext()->getFile();
		$line_number = $function->getContext()->getLineNumberStart();

		// Check we don’t already computed this call
		if( in_array( $uniqueKey, $this->seenContexts ) ) {
			return;
		}

		// Open the existing file
		$json = [];
		$json_dir = $this->calls_directory . '/by-class/' . $class_name;
		$json_file = $json_dir . '/' . $method_name . '.json';
		if( !is_dir( $json_dir ) ) {
			mkdir( $json_dir, 0777, true );
		}
		if( is_file( $json_file ) ) {
			$json = json_decode( file_get_contents( $json_file ), true );
		} else {
			$captured_definition = $this->captureFunctionDefinitionCode(
				$file_name,
				$line_number,
				$method_name
			);
			$json = [
				'definition' => [
					'file' => $file_name,
					'line_start' => $line_number,
					'column_start' => $captured_definition ? $captured_definition[2] : null,
					'byte_start' => $captured_definition ? $captured_definition[3] : null,
					'line_end' => $captured_definition ? $captured_definition[4] : null,
					'column_end' => $captured_definition ? $captured_definition[5] : null,
					'byte_end' => $captured_definition ? $captured_definition[6] : null,
					'class' => $class_name,
					'method' => $method_name,
					'code' => $captured_definition ? $captured_definition[0] : null,
					'line_before_capture' => $captured_definition ? $captured_definition[7] : null,
					'line_after_capture' => $captured_definition ? $captured_definition[8] : null,
					'isMethod' => $is_method,
					'isInterface' => $is_method && $class->isInterface(),
					'isAbstract' => $is_method && $method_definition->isAbstract(),
					'isFinal' => $is_method && $method_definition->isFinal(),
					'isOverride' => $is_method && $method_definition->isOverride(),
					'isOverridenByAnother' => $is_method && $method_definition->isOverriddenByAnother(),
					'overriddenMethods' => $is_method ? array_map( function( $x ) {
						return (string) $x->getFQSEN();
					}, $method_definition->getOverriddenMethods( $code_base ) ) : [],
					'possibleOverrides' => $is_method ? array_map( function( $x ) {
						return (string) $x->getFQSEN();
					}, $method_definition->getPossibleOverrides( $code_base ) ) : [],
					'definingFQSEN' => $is_method && $method_definition->hasDefiningFQSEN() ? (string) $method_definition->getDefiningFQSEN() : null,
					'realDefiningFQSEN' => $is_method && $method_definition->getRealDefiningFQSEN() ? (string) $method_definition->getRealDefiningFQSEN() : null,
				],
				'calls' => [],
			];

			$json_link_dir = $this->calls_directory . '/by-file/' . $file_name;
			if( $captured_definition && $captured_definition[3] ) {
				$json_link = $json_link_dir . '/' . $captured_definition[3] . '.json';
				if( !is_link( $json_link ) ) {
					if( !is_dir( $json_link_dir ) ) {
						mkdir( $json_link_dir, 0777, true );
					}
					symlink( $json_file, $json_link );
				}
			}

			// Register this class and method in the global list
			if( preg_match( '/^extensions\//', $file_name ) ) {
				$category = 'extensions';
			} elseif( preg_match( '/^skins\//', $file_name ) ) {
				$category = 'skins';
			} elseif( preg_match( '/^vendor\//', $file_name ) ) {
				$category = 'vendor';
			} else {
				$category = 'MediaWiki';
			}
			if( !in_array( $class_name, $this->class_names[$category] ) ) {
				$this->class_names[$category][] = $class_name;
				$this->method_names[$class_name] = [];
			}
			if( !in_array( $method_name, $this->method_names[$class_name] ) ) {
				$this->method_names[$class_name][] = $method_name;
			}
		}

		$astReverted = ASTReverter::toShortString( $node );
		$captured_code = null;
		$calling_code = null;
		$r = preg_match( '/^[^(]+/', $astReverted, $matches );
		if( $r ) {
			$calling_code = $matches[0];
		}
		if( $calling_code ) {
			$captured_code = $this->captureFunctionCallCode( $context->getFile(), $context->getLineNumberStart(), $calling_code );
		}

		if( $captured_code ) {
			$json['calls'][] = [
				'file' => $context->getFile(),
				'line_start' => $captured_code[1],
				'column_start' => $captured_code[2],
				'byte_start' => $captured_code[3],
				'line_end' => $captured_code[4],
				'column_end' => $captured_code[5],
				'byte_end' => $captured_code[6],
				'code' => $captured_code[0],
				'line_before_capture' => $captured_code[7],
				'line_after_capture' => $captured_code[8],
				'lines_before3' => $captured_code[9],
				'lines_after3' => $captured_code[10],
			];
		} else {
			$json['calls'][] = [
				'file' => $context->getFile(),
				'line_start' => $context->getLineNumberStart(),
				'column_start' => null,
				'byte_start' => null,
				'line_end' => null,
				'column_end' => null,
				'byte_end' => null,
				'code' => $astReverted,
				'line_before_capture' => null,
				'line_after_capture' => null,
				'lines_before3' => null,
				'lines_after3' => null,
			];
		}

		// \Phan\Language\FileRef::$line_number_end seems to be always the end of the file
		//$serializedContext = $context->__serialize();
		//if( is_array( $serializedContext ) && count( $serializedContext ) >= 3 && array_key_exists( 2, $serializedContext ) ) {
		//	$lineEnd = $serializedContext[2];
		//	if( $lineEnd !== $context->getLineNumberStart() ) {
		//		$githubLink .= '-L' . $lineEnd;
		//	}
		//}

		file_put_contents( $json_file, json_encode( $json ) );

		// Register this call to avoid duplicates
		$this->seenContexts[] = $uniqueKey;
	}

	public function analyzeFunction( CodeBase $code_base, Func $function ) : void {
	}

	public function analyzeMethod( CodeBase $code_base, Method $method ) : void {
		$method_fqsen = $method->getContext()->getFunctionLikeFQSEN();
		$method_definition = $method->getContext()->getFunctionLikeInScope( $code_base );
		$is_method = $method_definition instanceof Method;
		$method_name = $method_fqsen->getName();
		$class_fqsen = $is_method ? $method_fqsen->getFullyQualifiedClassName() : null;
		$class_name = $is_method ? preg_replace( '/^\//', '', str_replace( '\\', '/', (string) $class_fqsen ) ) : null;
		$class = $is_method ? $method_definition->getClass( $code_base ) : null;
		$file_name = $method->getContext()->getFile();
		$line_number = $method->getContext()->getLineNumberStart();

		// Open the existing file
		$json = [];
		$json_dir = $this->calls_directory . '/by-class/' . $class_name;
		$json_file = $json_dir . '/' . $method_name . '.json';
		if( !is_dir( $json_dir ) ) {
			mkdir( $json_dir, 0777, true );
		}
		if( is_file( $json_file ) ) {
			$json = json_decode( file_get_contents( $json_file ), true );
		} else {
			$captured_definition = $this->captureFunctionDefinitionCode(
				$file_name,
				$line_number,
				$method_name
			);
			$json = [
				'definition' => [
					'file' => $file_name,
					'line_start' => $line_number,
					'column_start' => $captured_definition ? $captured_definition[2] : null,
					'byte_start' => $captured_definition ? $captured_definition[3] : null,
					'line_end' => $captured_definition ? $captured_definition[4] : null,
					'column_end' => $captured_definition ? $captured_definition[5] : null,
					'byte_end' => $captured_definition ? $captured_definition[6] : null,
					'class' => $class_name,
					'method' => $method_name,
					'code' => $captured_definition ? $captured_definition[0] : null,
					'line_before_capture' => $captured_definition ? $captured_definition[7] : null,
					'line_after_capture' => $captured_definition ? $captured_definition[8] : null,
					'isMethod' => $is_method,
					'isInterface' => $is_method && $class->isInterface(),
					'isAbstract' => $is_method && $method_definition->isAbstract(),
					'isFinal' => $is_method && $method_definition->isFinal(),
					'isOverride' => $is_method && $method_definition->isOverride(),
					'isOverridenByAnother' => $is_method && $method_definition->isOverriddenByAnother(),
					'overriddenMethods' => $is_method ? array_map( function( $x ) {
						return (string) $x->getFQSEN();
					}, $method_definition->getOverriddenMethods( $code_base ) ) : [],
					'possibleOverrides' => $is_method ? array_map( function( $x ) {
						return (string) $x->getFQSEN();
					}, $method_definition->getPossibleOverrides( $code_base ) ) : [],
					'definingFQSEN' => $is_method && $method_definition->hasDefiningFQSEN() ? (string) $method_definition->getDefiningFQSEN() : null,
					'realDefiningFQSEN' => $is_method && $method_definition->getRealDefiningFQSEN() ? (string) $method_definition->getRealDefiningFQSEN() : null,
				],
				'calls' => [],
			];

			$json_link_dir = $this->calls_directory . '/by-file/' . $file_name;
			if( $captured_definition && $captured_definition[3] ) {
				$json_link = $json_link_dir . '/' . $captured_definition[3] . '.json';
				if( !is_link( $json_link ) ) {
					if( !is_dir( $json_link_dir ) ) {
						mkdir( $json_link_dir, 0777, true );
					}
					symlink( $json_file, $json_link );
				}
			}
		}

		// Register this class and method in the global list
		if( preg_match( '/^extensions\//', $file_name ) ) {
			$category = 'extensions';
		} elseif( preg_match( '/^skins\//', $file_name ) ) {
			$category = 'skins';
		} elseif( preg_match( '/^vendor\//', $file_name ) ) {
			$category = 'vendor';
		} else {
			$category = 'MediaWiki';
		}
		if( !in_array( $class_name, $this->class_names[$category] ) ) {
			$this->class_names[$category][] = $class_name;
			$this->method_names[$class_name] = [];
		}
		if( !in_array( $method_name, $this->method_names[$class_name] ) ) {
			$this->method_names[$class_name][] = $method_name;
		}

		file_put_contents( $json_file, json_encode( $json ) );
	}

	/**
	 * Hook each function call to further save it in the database.
	 *
	 * This is highly inspired from Phan\Analysis::loadMethodPlugins(),
	 * specifically the part about function calls, the main difference
	 * is that *all* function calls are hooked.
	 * The hook "beforeAnalyzePhase" is positionned just after the hook
	 * "AnalyzeFunctionCall" in Phan\Phan::finishAnalyzingRemainingStatements().
	 *
	 * @param CodeBase $code_base Code base to be analyzed
	 * @return array
	 */
	public function beforeAnalyzePhase( CodeBase $code_base ) : void {

		$closure = \Closure::fromCallable( [ $this, 'saveFunctionCall' ] );

        	$classes = $code_base->getUserDefinedClassMap();
        	foreach( $classes as $class_fqsen => $class ) {
			$isFinal = $class->isFinal();
			$isAbstract = $class->isAbstract();
			$isInterface = $class->isInterface();
			$isClass = $class->isClass();
			$isTrait = $class->isTrait();
			$interfaces = $class->getInterfaceFQSENList();
			foreach( $class->getInterfaceFQSENList() as $interface_fqsen ) {
				if( !$classes->offsetExists( $interface_fqsen ) ) {
					echo "Unknown interface " . ((string) $interface_fqsen) . "\n";
					continue;
				}
				$interface = $classes[$interface_fqsen];
				if( !$interface->isInterface() ) {
					echo "Interface " . ((string) $interface_fqsen) . " is not an interface\n";
					continue;
				}
				if( !array_key_exists( (string) $interface_fqsen, $this->classesByInterface ) ) {
					$this->classesByInterface[(string) $interface_fqsen] = [];
				}
				$this->classesByInterface[(string) $interface_fqsen][] = (string) $class_fqsen;
			}
			foreach( $class->getMethodMap( $code_base ) as $method_fqsen => $method ) {
				$method->addFunctionCallAnalyzer( $closure );
				/*
				// If I understand correctly, this is to register functions defined inside the method?
                            	$methods_by_defining_fqsen = $methods_by_defining_fqsen ?? $code_base->getMethodsMapGroupedByDefiningFQSEN();
                            	$fqsen = FullyQualifiedMethodName::fromFullyQualifiedString( $method_fqsen );
				if( !$methods_by_defining_fqsen->offsetExists( $fqsen ) ) {
					continue;
				}

				foreach( $methods_by_defining_fqsen->offsetGet( $fqsen ) as $child_method ) {
					$child_method->addFunctionCallAnalyzer( $closure );
				}
				*/
			}
		}
	}

	/**
	 * Capture the PHP code of the function definition.
	 *
	 * The AST only returns the start line but not the last line.
	 * This function tries to get this last line.
	 *
	 * If successful, the resulting array is:
	 * [ $extracted_code,
	 *   $start_character, $start_line, $start_column,
	 *   $end_character, $end_line, $end_column ],
	 * where characters are 0-indexed,
	 * and lines and columns are 1-indexed.
	 * If not successful, null is returned.
	 *
	 * @param string $file_path
	 * @param int $line_number_start
	 * @param string $name Regex of the starting element
	 * @return array|null
	 */
	function captureFunctionDefinitionCode( $file_path, $line_number_start, $name ) {

		// Some methods are internal, e.g. \Exception::__construct()
		if( $file_path === 'internal' ) {
			return null;
		}

		$file_content = file_get_contents( $this->code_base_directory . '/' . $file_path );
		$lines = explode( "\n", $file_content );
		$total_bytes = strlen( $file_content );

		# Count the number of characters (in fact bytes) on each line
		$lines_to_bytes = [];
		$bytes = 0;
		for( $i = 0; $i < count( $lines ); $i++ ) {
			$lines_to_bytes[$i] = $bytes;
			$bytes += strlen( $lines[$i] ) + 1;
		}
		$lines_to_bytes[$i] = $bytes;
		if( $bytes - 1 !== $total_bytes ) {
			throw new \LogicException();
		}
		$byte_number_start = null;
		$column_number_start = null;
		$line_before_capture = null;
		$line_after_capture = null;

		$escaped_name = preg_quote( $name, '/' );
		$r = preg_match( "/(?:(?:public|protected|private|static|abstract|final) +)*function +${escaped_name}[ \n]*\([^)]*\)[ \n]*(?::[ \n]*[a-zA-Z_\\\\]+[ \n]*)?{/", $file_content, $matches, PREG_OFFSET_CAPTURE, $lines_to_bytes[$line_number_start-1] );
		if( !$r ) {
			return null;
		}
		$column_number_start = $matches[0][1] - $lines_to_bytes[$line_number_start-1] + 1;
		$byte_number_start = $matches[0][1];
		$cursor = $byte_number_start;

		try {
			list( $cursor, $opened_parentheses ) = Utils::recursive_search( $cursor + strlen( $matches[0][0] ), 1, '{', '}', $file_content, $total_bytes );
		} catch( \RuntimeException $e ) {
			return null;
		} catch( \LogicException $e ) {
			return null;
		}

		$byte_number_end = $cursor;
		$extracted_code = substr( $file_content, $byte_number_start, $byte_number_end - $byte_number_start );
		$line_number_end = $line_number_start + count( explode( "\n", $extracted_code ) ) - 1;
		$column_number_end = $byte_number_end - $lines_to_bytes[$line_number_end-1] + 1;
		$line_before_capture = substr( $file_content, $lines_to_bytes[$line_number_start-1], $byte_number_start - $lines_to_bytes[$line_number_start-1] );
		$line_after_capture = substr( $file_content, $byte_number_end, $lines_to_bytes[$line_number_end] - 1 - $byte_number_end );

		return [
			$extracted_code,
			$line_number_start,
			$column_number_start,
			$byte_number_start,
			$line_number_end,
			$column_number_end,
			$byte_number_end,
			$line_before_capture,
			$line_after_capture,
		];
	}

	/**
	 * Capture the PHP code of the function call.
	 *
	 * The AST only returns the start line but not the last line.
	 * This function tries to get this last line.
	 *
	 * If successful, the resulting array is:
	 * [ $extracted_code,
	 *   $start_character, $start_line, $start_column,
	 *   $end_character, $end_line, $end_column ],
	 * where characters are 0-indexed,
	 * and lines and columns are 1-indexed.
	 * If not successful, null is returned.
	 *
	 * @param string $file_path
	 * @param int $line_number_start
	 * @param string $name Regex of the starting element
	 * @return array|null
	 */
	function captureFunctionCallCode( $file_path, $line_number_start, $name ) {

		static $last_file_path = null, $last_file_content = null, $last_total_bytes = null, $last_lines_to_bytes = null;

		if( $file_path === $last_file_path ) {

			$file_content = $last_file_content;
			$total_bytes = $last_total_bytes;
			$lines_to_bytes = $last_lines_to_bytes;

		} else {

			$file_content = file_get_contents( $this->code_base_directory . '/' . $file_path );
			$lines = explode( "\n", $file_content );
			$total_bytes = strlen( $file_content );

			# Count the number of characters (in fact bytes) on each line
			$lines_to_bytes = [];
			$bytes = 0;
			for( $i = 0; $i < count( $lines ); $i++ ) {
				$lines_to_bytes[$i] = $bytes;
				$bytes += strlen( $lines[$i] ) + 1;
			}
			$lines_to_bytes[$i] = $bytes;
			if( $bytes - 1 !== $total_bytes ) {
				throw new \LogicException();
			}

			// Cache
			$last_file_path = $file_path;
			$last_file_content = $file_content;
			$last_total_bytes = $total_bytes;
			$last_lines_to_bytes = $lines_to_bytes;
		}
		$byte_number_start = null;
		$column_number_start = null;
		$line_before_capture = null;
		$line_after_capture = null;

		$escaped_name = preg_quote( $name, '/' );
		$r = preg_match( "/${escaped_name}[ \n]*\(/", $file_content, $matches, PREG_OFFSET_CAPTURE, $lines_to_bytes[$line_number_start-1] );
		if( !$r ) {
			return null;
		}
		$column_number_start = $matches[0][1] - $lines_to_bytes[$line_number_start-1] + 1;
		$byte_number_start = $matches[0][1];
		$cursor = $byte_number_start;

		try {
			list( $cursor, $opened_parentheses ) = Utils::recursive_search( $cursor + strlen( $matches[0][0] ), 1, '(', ')', $file_content, $total_bytes );
		} catch( \RuntimeException $e ) {
			return null;
		} catch( \LogicException $e ) {
			return null;
		}

		$byte_number_end = $cursor;
		$extracted_code = substr( $file_content, $byte_number_start, $byte_number_end - $byte_number_start );
		$line_number_end = $line_number_start + count( explode( "\n", $extracted_code ) ) - 1;
		$column_number_end = $byte_number_end - $lines_to_bytes[$line_number_end-1] + 1;
		$line_before_capture = substr( $file_content, $lines_to_bytes[$line_number_start-1], $byte_number_start - $lines_to_bytes[$line_number_start-1] );
		$line_after_capture = substr( $file_content, $byte_number_end, $lines_to_bytes[$line_number_end] - 1 - $byte_number_end );
		$line_number_start_context3 = max( $line_number_start-1 - 3, 0 );
		$line_number_end_context3 = min( $line_number_end + 3, count( $lines_to_bytes ) - 1  );
		$lines_before3 = substr( $file_content, $lines_to_bytes[$line_number_start_context3], $lines_to_bytes[$line_number_start-1] - 1 - $lines_to_bytes[$line_number_start_context3] );
		$lines_after3 = substr( $file_content, $lines_to_bytes[$line_number_end], $lines_to_bytes[$line_number_end_context3] - 1 - $lines_to_bytes[$line_number_end] );

		return [
			$extracted_code,
			$line_number_start,
			$column_number_start,
			$byte_number_start,
			$line_number_end,
			$column_number_end,
			$byte_number_end,
			$line_before_capture,
			$line_after_capture,
			$lines_before3,
			$lines_after3,
		];
	}
}

return new PhanSearchPlugin();

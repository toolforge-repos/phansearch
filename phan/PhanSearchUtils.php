<?php

declare(strict_types=1);

namespace Phan\Plugin\PhanSearch;

class Utils {

	/**
	 * Get Git hash of a repository.
	 *
	 * @param string $dir Directory
	 * @return string|null Git hash or null in case of failure
	 */
	static function getGitHash( string $dir ) {

		$git_dir = "$dir/.git";
		while( is_file( $git_dir ) ) {
			$content = file_get_contents( $git_dir );
			if( preg_match( '/^gitdir: (.+)$/', $content, $matches ) ) {
				$git_dir = realpath( $git_dir . '/' . $matches[1] );
			}
		}
		if( !is_dir( $git_dir ) ) {
			return null;
		}

		# Read HEAD
		$head = file_get_contents( $git_dir . '/HEAD' );
		$ref = null;
		if( preg_match( '/^ref: (.+)$/', $head, $matches ) ) {
			$ref = $matches[1];
		} elseif( preg_match( '/^([0-9a-f]{40})$/', $head, $matches ) ) {
			return $matches[1];
		} else {
			return null;
		}

		# Read ref in directory .git/refs
		if( is_file( "$git_dir/$ref" ) ) {
			$ref = file_get_contents( "$git_dir/$ref" );
			if( preg_match( "/^([0-9a-f]{40})$/", $ref, $matches ) ) {
				return $matches[1];
			} else {
				return null;
			}
		}

		# Read ref in .git/packed-refs
		if( is_file( "$git_dir/packed-refs" ) ) {
			$packed_refs = file_get_contents( "$git_dir/packed-refs" );
			if( $packed_refs && preg_match( '/^([0-9a-f]{40}) ' . preg_quote( $ref, '/' ) . '$/m', $packed_refs, $matches ) ) {
				return $matches[1];
			}
		}

		return null;
	}

	/**
	 * @param int $cursor Current cursor (index of bytes in the string)
	 * @param int $opened_parentheses Current number of opened parentheses (at the start of the function call)
	 * @param string $file_content Content of the file to be searched (constant in this function)
	 * @param int $total_bytes Total number of bytes in the file (constant in this function)
	 * @return array Tuple of ($new_cursor, $new_opened_parentheses)
	 * @throws \LogicException|\RuntimeException
	 */
	static function recursive_search( $cursor, $opened_parentheses, $open_parenthesis, $close_parenthesis, $file_content, $total_bytes ) {
		if( $opened_parentheses === 0 ) {
			return [ $cursor, $opened_parentheses ];
		}
		if( $opened_parentheses >= 50 ) {
			throw new \RuntimeException( 'depth >= 50' );
		}
		$twoParentheses = $open_parenthesis . $close_parenthesis;

		while( true ) {
			if( $cursor > $total_bytes ) {
				throw new \RuntimeException( 'cursor > total_bytes' );
			}
			$r = preg_match( '/[^' . $twoParentheses . '\'"\/#<]*/A', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
			if( !$r ) {
				throw new \LogicException( 'Failed general regex in recursive_search' );
			}
			$cursor = $matches[0][1] + strlen( $matches[0][0] );
			$current_character = substr( $file_content, $cursor, 1 );
			if( $current_character === '/' ) {
				if( substr( $file_content, $cursor, 2 ) === '//' ) {
					$r = preg_match( '/\/\/[^\n]*\n?/A', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
					if( !$r ) {
						throw new \LogicException( 'Failed regex // in recursive_search' );
					}
					$cursor = $matches[0][1] + strlen( $matches[0][0] );
				} elseif( substr( $file_content, $cursor, 2 ) === '/*' ) {
					$r = preg_match( '/\/\*(?:[^\*]+|\*(?!\/))*\*\//A', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
					if( !$r ) {
						throw new \LogicException( 'Failed regex /* in recursive_search' );
					}
					$cursor = $matches[0][1] + strlen( $matches[0][0] );
				} else {
					$cursor += 1;
				}
			} elseif( $current_character === '<' ) {
				$r = preg_match( '/<<<([a-zA-Z0-9_]+)\n/A', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
				if( $r ) {
					$word = preg_quote( $matches[1][0], '/' );
					$cursor = $matches[0][1] + strlen( $matches[0][0] );
					$r = preg_match( '/(?<=\n)' . $word . ';?\n/', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
					if( !$r ) {
						throw new \LogicException( 'Failed regex in heredoc in recursive_search' );
					}
					$cursor = $matches[0][1] + strlen( $matches[0][0] );
				} else {
					$cursor += 1;
				}
			} elseif( $current_character === '#' ) {
				$r = preg_match( '/#[^\n]*\n?/A', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
				if( !$r ) {
					throw new \LogicException( 'Failed regex # in recursive_search' );
				}
				$cursor = $matches[0][1] + strlen( $matches[0][0] );
			} elseif( $current_character === '\'' ) {
				$r = preg_match( '/\'(?:[^\\\\\']+|\\\\.)*\'/A', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
				if( !$r ) {
					throw new \LogicException( 'Failed regex \' in recursive_search' );
				}
				$cursor = $matches[0][1] + strlen( $matches[0][0] );
			} elseif( $current_character === '"' ) {
				$r = preg_match( '/"(?:[^\\\\"]+|\\\\.)*"/A', $file_content, $matches, PREG_OFFSET_CAPTURE, $cursor );
				if( !$r ) {
					throw new \LogicException( 'Failed regex " in recursive_search' );
				}
				$cursor = $matches[0][1] + strlen( $matches[0][0] );
			} elseif( $current_character === $open_parenthesis ) {
				$opened_parentheses++;
				$cursor++;
				list( $cursor, $opened_parentheses ) = self::recursive_search( $cursor, $opened_parentheses, $open_parenthesis, $close_parenthesis, $file_content, $total_bytes );
				if( $opened_parentheses === 0 ) {
					return [ $cursor, $opened_parentheses ];
				}
			} elseif( $current_character === $close_parenthesis ) {
				$opened_parentheses--;
				$cursor++;
				list( $cursor, $opened_parentheses ) = self::recursive_search( $cursor, $opened_parentheses, $open_parenthesis, $close_parenthesis, $file_content, $total_bytes );
				if( $opened_parentheses === 0 ) {
					return [ $cursor, $opened_parentheses ];
				}
			} else {
				throw new \LogicException( 'Failed condition in recursive_search' );
			}
		}
	}
}
